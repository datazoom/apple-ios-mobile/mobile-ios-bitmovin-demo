//
//  ViewController.swift
//  BitmovinDemo
//
//  Created by MAC on 05/08/18.
//  Copyright © 2018 MAC. All rights reserved.
//

import UIKit
import BitmovinPlayer

var globalAssetValues : String!

class ViewController: UIViewController, UITextViewDelegate {
       
    var configId:String         = ""
    var connectionURL:String    = ""

    let devURL:String           = "https://devplatform.datazoom.io/beacon/v1/"
    let qaURL:String            = "https://stagingplatform.datazoom.io/beacon/v1/"
    let preProductionURL:String = "https://demoplatform.datazoom.io/beacon/v1/"
    let productionURL:String    = "https://platform.datazoom.io/beacon/v1/"

    @IBOutlet weak var videoOneEvent        : UIButton!
    @IBOutlet weak var fieldConfigId        : UITextView!
    @IBOutlet weak var connectingURLLabel   : UILabel!
    @IBOutlet weak var envrmntSegment       : UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.fieldConfigId.text = "0afb8d03-4750-4dd0-ae23-d9dd14f84d29"
        self.fieldConfigId.delegate = self
        self.configId = fieldConfigId.text
        self.connectingURLLabel.text = devURL
        self.connectionURL = devURL
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

    func validateConfigId(configIdL:String) -> Bool {
        if configIdL.range(of: "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}", options: .regularExpression, range: nil, locale: nil) != nil {
            return true
        }else {
            return false
        }
    }
    
    @IBAction func videoOneEvent(_ sender: Any) {
        if validateConfigId(configIdL: self.configId) {
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PlayerViewController") as? PlayerViewController
            vc?.configId = self.configId
            vc?.connectionURL = self.connectionURL
            self.navigationController?.pushViewController(vc!, animated: true)
        }else {
            self.showInvalidConfigIdAlert()
            return
        }
        globalAssetValues = "http://thenewcode.com/assets/videos/after.mp4"
    }
    
    func showInvalidConfigIdAlert() {
        let alert = UIAlertController(title: "", message: "Please enter a valid Configuration Id.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.configId = fieldConfigId.text
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    @IBAction func didChangeEnvironment(_ sender: Any) {
        switch envrmntSegment.selectedSegmentIndex {
        case 0:
            self.connectionURL = self.devURL
            print("Dev selected \(self.connectionURL)")
            self.connectingURLLabel.text = self.connectionURL
        case 1:
            self.connectionURL = self.qaURL
            print("QA selected \(self.connectionURL)")
            self.connectingURLLabel.text = self.connectionURL
        case 2:
            self.connectionURL = self.preProductionURL
            print("Pre Production selected \(self.connectionURL)")
            self.connectingURLLabel.text = self.connectionURL
        case 3:
            self.connectionURL = self.productionURL
            print("Production selected \(self.connectionURL)")
            self.connectingURLLabel.text = self.connectionURL
        default:
            break
        }
    }
    
    
  
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

