# bitmovin-player-ios-demo
This repository contains demo app which are using the Bitmovin Player iOS SDK, and the DataZoom library to capture events from the bitmovin player.


## Running the Bitmovin Demo app using Xcode:

* Clone the Project from **[Here](https://gitlab.com/datazoom/mobile-ios-bitmovin-demo/tree/bitmovin_demo_develop)**

* Open the project (Double click the project file with extension .xcodeproj)

* Select the required Simulator from top left in Xcode
* Click the Play button given on top left corner in Xcode to run the Project

![Screenshot pane](Xcode_ScreenShot.png)

* This will run the project and open the app in selected Simulator
* From simlator, the app can be tested by entering config id.
* You can choose between QA/Dev/Production environment. Make sure that you enter correct config id according to the environment selected.

![Screenshot pane](App_ScreenShot.png)

* You can observe the events generated and sent to connectors from the log in Xcode or you can check in corresponding connectors.

## Events and Metrics :

* Bitmovin Player events and metrics are currently captured by the Datazoom iOS platform.

## Dependencies

* Make sure that you use Xcode Version 9.4.1    

* If you want to use the Framework alone, You can download it from **[Here](https://gitlab.com/datazoom/apple-ios-mobile/mobile-ios-collector-frameworks/tree/master/bitmovin-collector)** .

* For Instructions on how to use the Framework, You can refer **[Here](https://gitlab.com/datazoom/mobile-ios-group/mobile-ios-bitmovin-collector/blob/develop/README.md)** .   

## Credit

- Vishnu M P

## Link to License/Confidentiality Agreement
Datazoom, Inc ("COMPANY") CONFIDENTIAL
Copyright (c) 2017-2018 [Datazoom, Inc.], All Rights Reserved.
NOTICE:  All information contained herein is, and remains the property of COMPANY. The intellectual and technical concepts contained
herein are proprietary to COMPANY and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
from COMPANY.  Access to the source code contained herein is hereby forbidden to anyone except current COMPANY employees, managers or contractors who have executed
Confidentiality and Non-disclosure agreements explicitly covering such access.
The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
information that is confidential and/or proprietary, and is a trade secret, of  COMPANY.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.



## Using The Bitmovin Player iOS SDK
When you want to develop an own iOS application using the Bitmovin Player iOS SDK read through the following steps.

### Adding the SDK To Your Project
To add the SDK as a dependency to your project, you have two options: Using CocoaPods or adding the SDK bundle directly.


#### Using CocoaPods
Add `pod 'BitmovinPlayer', git: 'https://github.com/bitmovin/bitmovin-player-ios-sdk-cocoapod.git', tag: '2.10.0'` to your Podfile. After that, install the pod using `pod install`. See the `Podfile` of this repository for a full example.

#### Adding the SDK Directly
+   When using XCode, go to the `General` settings page and add the SDK bundle (`BitmovinPlayer.framework`) under `Linked Frameworks and Libraries`. The SDK bundle can be downloaded from the [release page of the GitHub repository](https://github.com/bitmovin/bitmovin-player-ios-sdk-cocoapod/releases).

### Project Setup

+   Add your Bitmovin player license key to the `Info.plist` file as `BitmovinPlayerLicenseKey`.

    Your player license key can be found when logging in into [https://dashboard.bitmovin.com](https://dashboard.bitmovin.com) and navigating to `Player -> Licenses`.

+   Add the Bundle identifier of the iOS application which is using the SDK as an allowed domain to the Bitmovin licensing backend. This can be also done under `Player -> Licenses` when logging in into [https://dashboard.bitmovin.com](https://dashboard.bitmovin.com) with your account.

    When you do not do this, you'll get a license error when starting the application which contains the player.

## Documentation And Release Notes
-   You can find the latest API documentation [here](https://bitmovin.com/ios-sdk-documentation/)
-   The release notes can be found [here](https://bitmovin.com/release-notes-ios-sdk/)

